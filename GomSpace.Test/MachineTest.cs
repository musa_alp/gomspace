using GomSpace.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GomSpace.Test
{
    [TestClass]
    public class MachineTest
    {
        [TestMethod]
        public void MoveForward_WhenCall_ShouldMoveForwardOnXAxis()
        {
            //Arrange
            var machineX = 5;
            var machineY = 5;
            var grid = new Grid(10, 10);
            var machine = new Machine(machineX, machineY, Machine.Orieantation_Right);

            //Action
            machine.MoveForward(grid);

            //Assertion
            Assert.AreEqual(machineX + 1, machine.X);
        }

        [TestMethod]
        public void MoveForward_WhenCall_ShouldMoveBackwardOnXAxis()
        {
            //Arrange
            var machineX = 5;
            var machineY = 5;
            var grid = new Grid(10, 10);
            var machine = new Machine(machineX, machineY, Machine.Orieantation_Left);

            //Action
            machine.MoveForward(grid);

            //Assertion
            Assert.AreEqual(machineX - 1, machine.X);
        }

        [TestMethod]
        public void MoveForward_WhenCall_ShouldMoveUpwardOnYAxis()
        {
            //Arrange
            var machineX = 5;
            var machineY = 5;
            var grid = new Grid(10, 10);
            var machine = new Machine(machineX, machineY, Machine.Orieantation_Up);

            //Action
            machine.MoveForward(grid);

            //Assertion
            Assert.AreEqual(machineY - 1, machine.Y);
        }

        [TestMethod]
        public void MoveForward_WhenCall_ShouldMoveDownwardOnYAxis()
        {
            //Arrange
            var machineX = 5;
            var machineY = 5;
            var grid = new Grid(10, 10);
            var machine = new Machine(machineX, machineY, Machine.Orieantation_Down);

            //Action
            machine.MoveForward(grid);

            //Assertion
            Assert.AreEqual(machineY + 1, machine.Y);
        }

        [TestMethod]
        public void MoveForward_FiveTimeCall_IncreaseStepNumberByFive()
        {
            //Arrange
            var grid = new Grid(10, 10);
            var machine = new Machine(5, 5, Machine.Orieantation_Down);

            //Action
            machine.MoveForward(grid);
            machine.MoveForward(grid);
            machine.MoveForward(grid);
            machine.MoveForward(grid);
            machine.MoveForward(grid);

            //Assertion
            Assert.AreEqual(5, machine.StepNumber);
        }

        [TestMethod]
        public void TurnRight_WhenCall_TurnRightFromDown()
        {
            //Arrange
            var machineX = 5;
            var machineY = 5;

            var machine = new Machine(machineX, machineY, Machine.Orieantation_Down);

            //Action
            machine.TurnRight();

            //Assertion
            Assert.AreEqual(Machine.Orieantation_Left, machine.Orientation);
        }

        [TestMethod]
        public void TurnRight_WhenCall_TurnRightFromUp()
        {
            //Arrange
            var machineX = 5;
            var machineY = 5;

            var machine = new Machine(machineX, machineY, Machine.Orieantation_Up);

            //Action
            machine.TurnRight();

            //Assertion
            Assert.AreEqual(Machine.Orieantation_Right, machine.Orientation);
        }

        [TestMethod]
        public void TurnLeft_WhenCall_TurnLeftFromLeft()
        {
            //Arrange
            var machineX = 5;
            var machineY = 5;

            var machine = new Machine(machineX, machineY, Machine.Orieantation_Left);

            //Action
            machine.TurnLeft();

            //Assertion
            Assert.AreEqual(Machine.Orieantation_Down, machine.Orientation);
        }

        [TestMethod]
        public void TurnLeft_WhenCall_TurnLeftFromRight()
        {
            //Arrange
            var machineX = 5;
            var machineY = 5;

            var machine = new Machine(machineX, machineY, Machine.Orieantation_Right);

            //Action
            machine.TurnLeft();

            //Assertion
            Assert.AreEqual(Machine.Orieantation_Up, machine.Orientation);
        }
    }
}
