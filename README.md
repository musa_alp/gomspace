#GomSpace

GomSpace.App is a sample application built using .NET Core 2.2 Web Api, and Asp.Net Core MVC Application. The architecture and design of the project is explained below.

## Getting Started
Use these instructions to get the project up and running.

### Prerequisites
You will need the following tools:

* [Visual Studio 2017](https://www.visualstudio.com/downloads/)
* [.NET Core SDK 2.2](https://www.microsoft.com/net/download/dotnet-core/2.0)



Following nuget packages used in the project:

* Microsoft.AspNetCore.App 2.2.0
* Microsoft.AspNetCore.Razor.Design 2.2.0
* Microsoft.NET.Test.Sdk 15.7.0
* MSTest.TestAdapter 1.2.1
* MSTest.TestFramework 1.2.1
* iTextSharp.LGPLv2.Core 1.5.0


Following framework used in the project:

* AngularJS 1.5.5
* Bootstrap 3.3.7


### Setup
Follow these steps to get your development environment set up:


* Clone the repository

#### To Make Ready Web Api
* At the GomSpace.WebApi directory, restore required packages by running:
 
     ```
     dotnet restore
     ```
	 
* Next, build the solution by running:
 
     ```
     dotnet build
     ```	 
	 
* Launch the Web Api by running:
 
     ```
	 dotnet run
	 ```	 
	 
* It should be hosted at "http://localhost:5005". This url is configurable from GomSpace.WebApi > appsettings.json file. Grid size and machine position also be configure at this file. Default grid size is 100x100, machine position Y 50, X 50, orientation is right.

* Result should be similar to image linked below
 
* [Web Api Result Image](https://bitbucket.org/musa_alp/gomspace/src/master/web-api-commands.png)


#### To Make Ready Client App
* At the GomSpace.App directory, restore required packages by running:
 
     ```
     dotnet restore
     ```
     
* Next, build the solution by running:
 
     ```
     dotnet build
     ```     
     
* Launch the Client App by running:
 
     ```
     dotnet run
     ```     
     
* It should be run at "http://localhost:5000". This url is configurable from GomSpace.App > Properties > launchSettings.json file.

* Result should be similar to image linked below
 
* [App Result Image](https://bitbucket.org/musa_alp/gomspace/src/master/client-app-commands.png)

 
### Run Unit Tests

1. To run tests and get result of details. Go to GomSpace.Test directory then execute the line code blow. 

	 ```
	 dotnet test "GomSpace.Test.csproj" --logger "trx;LogFileName=..\..\TestResult\Results.trx"
	 ```
     
  
* Result should be similar to image linked below
 
* [Test Commands Result Image](https://bitbucket.org/musa_alp/gomspace/src/master/test-commands.png)

  After execution the test report will be exported to /TestResult directory in the root directory.

2. Open the application with Visual Studio 2017 at the Test Explorer panel click to RunAll. Result should be similar to image linked below
 
* [Unit Test Result Image](https://bitbucket.org/musa_alp/gomspace/src/master/test-results.png)
 

### Main Assumptions

* For every Http Put request machine will move one step according to rules.
* After move one step simulation will be run.
* The final version of the grid can be exported to a file. (I choice pdf format).
* Default grid size is 100x100, machine position Y 50, X 50, orientation is right.
* By focusing on main requirements, I tried not to extend the scope of the application too much. Like using, Orm, database, facade services, business layers etc.


### Limitations 
* To make move machine, Http request's method must be put.
* For the simulation at client app, firstly grid should be generated.
* When use "Move Multiple Step" there will be 50ms delay between steps. Its configurable with $scope.defaultMoveSpeed variable in GomSpace.App\wwwroot\js\controllers\machineController.js file.

### Potential Improvements.
#### Front-End
* Simulation start and stop behavior can be added.
* Adjust simulation speed behavior can be added.
* UI validation and understandable error message can be added to show user.
* Grid size, machine position and orientation can be adjustable from UI.

#### Back-End
* There should ve authentication mechanism while taking http request.
* Monitoring tools can be used for exceptions and logs.
* NoSql database can be used.
* Accident Management flow can be added
* Http request limitation can added for performance.
