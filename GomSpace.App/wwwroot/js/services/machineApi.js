﻿app.factory('machineApi',
    [
        'apiCaller', function (baseApi) {
            var url = 'http://localhost:5005/api/machine';
            var api = {};

            api.generateGrid = function (data, success, error) {
                baseApi.get(url + '/GenerateGrid', null, data, success, error);
            };

            api.move = function (data, success, error) {
                baseApi.put(url + '/Move', null, data, success, error);
            };

            return api;
        }
    ]);