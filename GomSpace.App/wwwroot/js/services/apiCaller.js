﻿app.factory('apiCaller',
    [
        '$http', '$window', function ($http, $window) {
            return {
                get: function (url, headers, data, success, error) {
                    headers = {};
                    $http({
                        method: "GET",
                        headers: headers,
                        params: data,
                        url: url
                    })
                        .then(function (response) {
                            success(response.data);
                        },
                            function (response) {
                                if (error) {
                                    error(response);
                                }
                            });
                },
                post: function (url, headers, data, success, error) {
                    headers = {};
                    $http({
                        method: "POST",
                        headers: headers,
                        dataType: "json",
                        data: data,
                        contentType: "application/json",
                        url: url
                    })
                        .then(function (response) {
                            success(response.data);
                        },
                            function (response) {
                                if (error) {
                                    error(response);
                                }
                            });
                },
                put: function (url, headers, data, success, error) {
                    headers = {};
                    $http({
                        method: "PUT",
                        headers: headers,
                        dataType: "json",
                        data: data,
                        contentType: "application/json",
                        url: url
                    })
                        .then(function (response) {
                            success(response.data);
                        },
                            function (response) {
                                if (error) {
                                    error(response);
                                }
                            });
                },
                delete: function (url, id, success, error) {
                    var headers = {};
                    $http({
                        method: "DELETE",
                        headers: headers,
                        url: url + "/" + id
                    })
                        .then(function (response) {
                            success(response.data);
                        },
                            function (response) {
                                if (error) {
                                    error(response.data);
                                }
                            });
                }
            }
        }
    ]);