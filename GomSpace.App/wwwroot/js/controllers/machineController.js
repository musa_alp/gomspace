﻿app.controller('machineController',
    [
        '$scope', 'machineApi', '$interval', function ($scope, machineApi, $interval) {

            $scope.defaultMoveSpeed = 50; //ms
            $scope.stepNumber = 0;
            $scope.surfaceModel = {};
            $scope.isGridGenerated = false;

            $scope.generateGrid = function () {
                var request = {};

                machineApi.generateGrid(request,
                    function (response) {
                        $scope.surfaceModel = response;
                        setArrowCss();
                        $scope.isGridGenerated = true;
                    },
                    function (response) {
                        console.log(response);
                    });
            };

            $scope.moveSingleStep = function () {
                var request = {};

                machineApi.move(request,
                    function (response) {
                        $scope.surfaceModel = response;
                        setArrowCss();
                    },
                    function (response) {
                        console.log(response);
                    });
            };

            //multiple call with defaultMoveSpeed delay
            $scope.moveMultipleStep = function () {
                if ($scope.stepNumber > 0) {
                    //internal call according to stepNumber
                    $interval($scope.moveSingleStep, $scope.defaultMoveSpeed, $scope.stepNumber);
                }
            };

            var setArrowCss = function () {
                $scope.arrowCss = "";

                for (var i = 0; i < $scope.surfaceModel.grid.height; i++) {
                    for (var j = 0; j < $scope.surfaceModel.grid.width; j++) {

                        if ($scope.surfaceModel.machine.y === i && $scope.surfaceModel.machine.x === j) {
                            if ($scope.surfaceModel.machine.orientation === 0) {
                                $scope.arrowCss = "glyphicon-circle-arrow-up";
                            }
                            else if ($scope.surfaceModel.machine.orientation === 1) {
                                $scope.arrowCss = "glyphicon-circle-arrow-left";
                            }
                            else if ($scope.surfaceModel.machine.orientation === 2) {
                                $scope.arrowCss = "glyphicon-circle-arrow-down";
                            }
                            else {
                                $scope.arrowCss = "glyphicon-circle-arrow-right";
                            }
                        }
                    }
                }
            }
        }
    ]);