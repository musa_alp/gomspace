﻿using GomSpace.App.Constans;
using Microsoft.AspNetCore.Mvc;

namespace GomSpace.App.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Url = ApiUrls.ExportToFileUrl;
            return View();
        }
    }
}
