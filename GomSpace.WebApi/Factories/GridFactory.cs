﻿using GomSpace.WebApi.Models;

namespace GomSpace.WebApi.Factories
{
    public class GridFactory
    {
        public static Grid Create(SurfaceSettings surfaceSettings)
        {
            return new Grid(surfaceSettings.Height, surfaceSettings.Width);
        }
    }
}
