﻿using GomSpace.WebApi.Models;

namespace GomSpace.WebApi.Factories
{
    public class MachineFactory
    {
        public static Machine Create(SurfaceSettings surfaceSettings)
        {
            return new Machine(surfaceSettings.MachineStartX, surfaceSettings.MachineStartY, surfaceSettings.MachineOrientation);
        }
    }
}
