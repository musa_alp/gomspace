﻿

using GomSpace.WebApi.Factories;
using GomSpace.WebApi.Models;
using Microsoft.Extensions.Options;
using System;

namespace GomSpace.WebApi.Helpers
{
    public class MachineManager : IMachineManager
    {
        private readonly SurfaceSettings _surfaceSettings;
        public MachineManager(IOptions<SurfaceSettings> options)
        {
            _surfaceSettings = options.Value;
        }

        public static Machine _machine;
        public static Machine Machine
        {
            get
            {
                if (_machine == null)
                {
                    throw new ArgumentNullException(nameof(_machine));
                }

                return _machine;
            }
        }

        public static Grid _grid;
        public static Grid Grid
        {
            get
            {
                if (_grid == null)
                {
                    throw new Exception("Grid must be generated");
                }

                return _grid;
            }
        }

        public MoveResponse Move(MoveRequest request)
        {
            if (Grid.Cells[Machine.Y, Machine.X] == Colors.White)
            {
                Grid.Cells[Machine.Y, Machine.X] = Colors.Black;
                Machine.TurnRight();
            }
            else
            {
                Grid.Cells[Machine.Y, Machine.X] = Colors.White;
                Machine.TurnLeft();
            }

            Machine.MoveForward(Grid);

            var response = new MoveResponse
            {
                Machine = Machine,
                Grid = Grid
            };

            return response;
        }

        public GenerateGridResponse GenerateGrid(GenerateGridRequest request)
        {
            _grid = GridFactory.Create(_surfaceSettings);
            _machine = MachineFactory.Create(_surfaceSettings);

            var response = new GenerateGridResponse
            {
                Grid = _grid,
                Machine = _machine
            };

            return response;
        }
    }

    public interface IMachineManager
    {
        MoveResponse Move(MoveRequest request);
        GenerateGridResponse GenerateGrid(GenerateGridRequest request);
    }
}
