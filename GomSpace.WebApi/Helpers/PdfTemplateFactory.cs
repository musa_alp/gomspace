﻿namespace GomSpace.WebApi.Helpers
{
    public class PdfTemplateFactory
    {
        public PdfTemplateFactory()
        {
        }

        public IPdfTemplateBuilder Create()
        {
            return new MachinePdfTemplate();
        }
    }
}
