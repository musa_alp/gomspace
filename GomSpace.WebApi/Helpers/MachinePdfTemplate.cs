﻿using GomSpace.WebApi.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;

namespace GomSpace.WebApi.Helpers
{
    public class MachinePdfTemplate : IPdfTemplateBuilder
    {
        private static BaseFont helvaticaTurkish = BaseFont.CreateFont("Helvetica", "CP1254", BaseFont.NOT_EMBEDDED);
        public Font tahoma = new Font(helvaticaTurkish, 4.3F, Font.NORMAL);
        public Font tahomaBold = new Font(helvaticaTurkish, 4.3F, Font.BOLD);
        public Font tahomaBig = new Font(helvaticaTurkish, 7.0F, Font.NORMAL);
        public Font tahomaBoldBig = new Font(helvaticaTurkish, 10.0F, Font.BOLD);

        public byte[] Build(object obj)
        {
            var surface = obj as MoveResponse;

            if (surface == null)
            {
                throw new ArgumentNullException(nameof(surface));
            }

            using (MemoryStream ms = new MemoryStream())
            {
                Document document = new Document(PageSize.A4, 15, 15, 40, 30);

                var paragraph = new Paragraph("GomSpace Assignment", tahomaBoldBig);

                PdfWriter writer = PdfWriter.GetInstance(document, ms);

                document.Open();
                document.Add(paragraph);
                document.Add(Chunk.Newline);
                document.Add(GetTableContent(surface));
                document.Close();

                writer.Close();
                return ms.ToArray();
            }
        }

        private IElement GetTableContent(MoveResponse moveResponse)
        {
            PdfPTable table = new PdfPTable(moveResponse.Grid.Width);

            table.WidthPercentage = 100;

            table.DefaultCell.Border = Rectangle.BOX;
            table.DefaultCell.BorderWidth = 0.1F;

            for (int i = 0; i < moveResponse.Grid.Height; i++)
            {
                for (int j = 0; j < moveResponse.Grid.Width; j++)
                {
                    var cell = new PdfPCell();
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.FixedHeight = 6f;

                    if (moveResponse.Grid.Cells[i, j] == 0 || moveResponse.Grid.Cells[i, j] == 2)
                    {
                        cell.BackgroundColor = BaseColor.White;
                        cell.BorderColor = BaseColor.Black;
                    }
                    else
                    {
                        cell.BackgroundColor = BaseColor.Black;
                        cell.BorderColor = BaseColor.White;
                    }

                    table.AddCell(cell);
                }
            }

            return table;
        }
    }

    public interface IPdfTemplateBuilder
    {
        byte[] Build(object obj);
    }
}
