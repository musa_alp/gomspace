﻿using GomSpace.WebApi.Helpers;
using GomSpace.WebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace GomSpace.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MachineController : ControllerBase
    {
        private readonly IMachineManager _machineManager;
        private static BaseResponse _response;
        public MachineController(IMachineManager machineManager)
        {
            _machineManager = machineManager;
        }

        // GET api/machine/generatedgrid
        [Route("GenerateGrid")]
        [HttpGet]
        public BaseResponse GenerateGrid()
        {
            _response = _machineManager.GenerateGrid(null);
            return _response;
        }

        // PUT api/machine/move        
        [Route("Move")]
        [HttpPut]
        public BaseResponse Move([FromBody]MoveRequest moveRequest)
        {
            _response = _machineManager.Move(moveRequest);
            return _response;
        }

        // GET api/machine/pdf/exporttofile
        [Route("pdf/ExportToFile")]
        [HttpGet]
        public IActionResult ExportToFile()
        {
            var pdfData = _response;

            var fileContent = new PdfTemplateFactory().Create().Build(pdfData);
            return File(fileContent, "application/pdf", "MachineGrid.pdf");
        }
    }
}
