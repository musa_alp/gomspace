﻿namespace GomSpace.WebApi.Models
{
    public class Grid
    {
        /// 0: inactive, 1: active, 2: inactive (visited)
        public Grid(int height, int width)
        {
            Height = height;
            Width = width;
            SetCellsAsWhiteColor();
        }

        public int Width { get; set; }
        public int Height { get; set; }
        public byte[,] Cells { get; set; }

        private void SetCellsAsWhiteColor()
        {
            Cells = new byte[Height, Width];

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    Cells[y, x] = Colors.White;
                }
            }
        }
    }
}
