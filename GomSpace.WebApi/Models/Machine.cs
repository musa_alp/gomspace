﻿namespace GomSpace.WebApi.Models
{
    public class Machine
    {
        public const sbyte Orieantation_Up = 0;
        public const sbyte Orieantation_Left = 1;
        public const sbyte Orieantation_Down = 2;
        public const sbyte Orieantation_Right = 3;

        public int X { get; set; }
        public int Y { get; set; }
        public sbyte Orientation { get; set; }
        public int StepNumber { get; set; }

        public Machine(int startX, int startY, sbyte startOrientation)
        {
            X = startX;
            Y = startY;
            Orientation = startOrientation;
        }

        public void MoveForward(Grid grid)
        {
            switch (Orientation)
            {
                case Orieantation_Up:
                    if (Y > 0)
                    {
                        Y--;
                    }
                    else
                    {
                        Y = grid.Height - 1;
                    }
                    break;
                case Orieantation_Left:
                    if (X > 0)
                    {
                        X--;
                    }
                    else
                    {
                        X = grid.Width - 1;
                    }
                    break;
                case Orieantation_Down:
                    if (Y < grid.Height - 1)
                    {
                        Y++;
                    }
                    else
                    {
                        Y = 0;
                    }
                    break;
                case Orieantation_Right:
                    if (X < grid.Width - 1)
                    {
                        X++;
                    }
                    else
                    {
                        X = 0;
                    }
                    break;
            }

            StepNumber++;
        }

        public void TurnLeft()
        {
            Orientation++;
            if (Orientation > 3)
                Orientation -= 4;
        }

        public void TurnRight()
        {
            Orientation--;
            if (Orientation < 0)
                Orientation += 4;
        }
    }
}
