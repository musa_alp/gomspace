﻿namespace GomSpace.WebApi.Models
{
    public class SurfaceSettings
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int MachineStartY { get; set; }
        public int MachineStartX { get; set; }
        public sbyte MachineOrientation { get; set; }
    }
}
