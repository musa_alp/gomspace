﻿namespace GomSpace.WebApi.Models
{
    public class GenerateGridRequest : BaseRequest
    {
    }

    public class GenerateGridResponse : BaseResponse
    {
    }

    public class MoveRequest : BaseRequest
    {
    }

    public class MoveResponse : BaseResponse
    {
    }

    public abstract class BaseRequest
    {
    }

    public abstract class BaseResponse
    {
        public Machine Machine { get; set; }
        public Grid Grid { get; set; }
    }
}
